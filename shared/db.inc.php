<?php

$dbname ="dbname";
$user = "user";
$password ="password";
$dbhost ="XXX.XXX.XXX.XXX";
$port = "XXXX";

try {
    $pdo = new PDO("mysql:host={$dbhost};port={$port};dbname={$dbname}", $user, $password, array(
      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    ));
  }
  catch(PDOException $e) {
    die("Konnte keine Verbindung mit Datenbank aufbauen");
}