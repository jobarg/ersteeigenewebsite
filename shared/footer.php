
<footer class="footer pt-md-3 pb-md-2">
  <div class="container-fluid text-center">
    <div class="row align-items-center">
      <div class="col-6 col-md-4 offset-md-0">
        <p class="mt-3"><a href="/impressum.php">Impressum</a></p>
      </div>
      <div class="col-6 col-md-4 offset-md-0">
        <p class="mt-3"><a href="/datenschutz.php">Datenschutz</a></p>
      </div>
      <div class="col-12 col-md-4">
        <div class="d-flex justify-content-center mb-3 mt-md-2">
          <a class="footer-icon d-flex flex-column justify-content-center" href="#">
            <i class="fab fa-twitter"></i>
          </a>
          <a class="footer-icon d-flex flex-column justify-content-center" href="#">
            <i class="fab fa-facebook"></i>
          </a>
          <a class="footer-icon d-flex flex-column justify-content-center" href="#">
            <i class="fab fa-pinterest"></i>
          </a>
          <a class="footer-icon d-flex flex-column justify-content-center" href="#">
            <i class="fab fa-google-plus-g"></i>
          </a>
          <a class="footer-icon d-flex flex-column justify-content-center" href="#">
            <i class="far fa-envelope"></i>
          </a>
        </div>
      </div>
    </div>
    <div class="row justify-content-center">
        <p><a href="index.php">&copy 2019 jbargen.de</a></p>      
    </div>
  </div>
</footer>