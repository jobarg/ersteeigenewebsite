<nav id="navbar-bewerbung2" class="navbar navbar-dark bg-dark sticky-top navbar-expand-sm">
    <a class="navbar-brand pl-3" href="../index.php"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-label="Toggle navigation" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" >
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
    <ul class="navbar-nav nav-pills">    
        <li class="nav-item">
        <a class="nav-link" href="#cover">Deckblatt</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#aboutme">Lebenslauf</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#thirdpage">Dritte Seite</a>
        </li>
        <li class="nav-item pr-3">
        <a class="nav-link" href="#attachment">Anhang</a>
        </li>
    </ul>
    </div>   
</nav>