<?php
/**Aufbau des Array
 * Alternativtext => Bildlink
 */
$startimage ="Zeugnis Studium"; //Key aus dem Array
$images =[
    "Zeugnis Studium" => "120104_small.png",
    "Abschlusszeugnis CSB" => "Abschlusszeugnis_small.png",
    "Blockchain Kurs" => "blockchain2018_small.png",
    "IPv6 Kurs" => "ipv6-2018_small.png",
    "MS Word Kurs" => "MS_Word_small.png",
    "CCNA NF" => "CCNA_NF_small.png",
    "CCNA WAN" => "CCNA_WAN_small.png"
];
?>

<section class="attachment" id="attachment">
    <h3 class="mb-5">Anhang</h3>
    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-interval="false" data-ride="carousel">
        <div class="carousel-inner">
            <?php foreach($images AS $image => $value): ?>
                 <?php if($image == $startimage):?>
                    <div class="carousel-item active">
                <?php else: ?>
                    <div class="carousel-item">
                <?php endif; ?>
                    <img src="../images/bewerbung/<?= $value; ?>" class="d-block img-fluid img-thumbnail m-auto" alt="<?=$image;?>">
                </div>                
            <?php endforeach; ?>
        </div>
        <a class="carousel-control-prev  text-red" href="#carouselExampleFade" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
  </div>
</section>