<?php require("./shared/functions.inc.php"); ?>
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">   
    <link rel="stylesheet" type="text/css" href="css/index.css" />
    
    <title>Erste eigene Webseite</title>
  </head>
  <body>
    <div id="page" class=""><!-- mobile-nav-opened -->
      <?php require("inc/header.php"); ?>
      <?php require("inc/overview.php"); ?>
      <?php require("inc/projects.php"); ?>
      <?php require("inc/contact.php"); ?>
      <?php require("./shared/footer.php"); ?> 
    </div>
    <?php include("inc/modal.php");?>
    <?php require("scripts/scripts.php"); ?>

  </body>
</html>