<!DOCTYPE html>
<html>
    <head>
        <!--<link rel="icon" type="image/vnd.microsoft.icon" href="../favicon.ico">-->
        <meta charset="utf-8"/>
        <meta content="width=device-width,initial-scale=1" name="viewport">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <title id="title">Bewerbung: <?php echo basename(realpath('.')); ?></title>        
        <link rel="stylesheet" href="../css/bewerbung/style.css"/>
        <!-- -->
        <script src="https://code.jquery.com/jquery-latest.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    </head>
    <body>
                        
        <div class="index">
            <?php 
            require("./inc/prevbutton.php"); 
            require("./inc/nextbutton.php");  
            require("./inc/deckblatt.php"); ?>
            <hr class="endofpage"/>
            <?php /*require(__DIR__."/anschreiben.php");*/ ?>
            <hr class="endofpage"/>
            <?php require("./inc/lebenslauf1.php"); ?>
            <hr class="endofpage"/>
            <?php require("./inc/dritteseite.php"); ?>
            <hr class="endofpage"/>
            <?php require("./inc/anhang.php"); ?>
            
        </div> 
        <?php 
            require("./inc/scripts.php");
        ?>
    </body>
</html> 