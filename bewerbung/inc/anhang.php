<img src="../images/bewerbung/120104_small.png" alt="Notenspiegel Studium" class="anhang" id="notenspiegel">
<hr class="endofpage"/>
<img src="../images/bewerbung/Abschlusszeugnis_small.png" alt="Abschlusszeugnis" class="anhang" id="abschluss">
<hr class="endofpage"/>
<img src="../images/bewerbung/Fachhochschulreife_small.png" alt="Fachhochschulreife" class="anhang" id="fachhoch">
<hr class="endofpage"/>
<img src="../images/bewerbung/INF_Assistent_small.png" alt="Inf. Assistent" class="anhang" id="assistent">  
<hr class="endofpage"/>  
<img src="../images/bewerbung/blockchain2018_small.png" alt="Blockchain Kurs" class="anhang" id="blockchain">
<hr class="endofpage"/>
<img src="../images/bewerbung/ipv6-2018_small.png" alt="IPv6 Kurs" class="anhang" id="ipv6">
<hr class="endofpage"/>
<img src="../images/bewerbung/MS_Word_small.png" alt="MS Office Specialist" class="anhang" id="word">
<hr class="endofpage"/>
<img src="../images/bewerbung/CCNA_NF_small.png" alt="Cisco NF Kurs" class="anhang" id="ccnanf">
<hr class="endofpage"/>
<img src="../images/bewerbung/CCNA_WAN_small.png" alt="Cisco WAN Kurs" class="anhang" id="ccnawan">

<div class="container" id="anhang">
    <h4 class="anhang-header">Anhang</h4>
    <div class="row">
        <div class="three columns">
            <a href="../images/bewerbung/120104_small.png" alt="Notenspiegel Studium" class="gallery-item" value="0">
                <img src="../images/bewerbung/120104_small.png" alt="0" />
            </a>
        </div>
        <div class="three columns">
            <a href="../images/bewerbung/Abschlusszeugnis_small.png" alt="Abschlusszeugnis" class="gallery-item" value="1">
                <img src="../images/bewerbung/Abschlusszeugnis_small.png" alt="1"/>
            </a>
        </div>
        <div class="three columns">
            <a href="../images/bewerbung/Fachhochschulreife_small.png" alt="Fachhochschulreife" class="gallery-item" value="2">
                <img src="../images/bewerbung/Fachhochschulreife_small.png" alt="2"/>
            </a>
        </div>
        <div class="three columns">
            <a href="../images/bewerbung/INF_Assistent_small.png" alt="Inf. Assistent" class="gallery-item" value="3">
                <img src="../images/bewerbung/INF_Assistent_small.png" alt="3" />
            </a>
        </div>   
    </div>     
    <div class="row">        
        <div class="three columns">
            <a href="../images/bewerbung/blockchain2018_small.png" alt="Blockchain Kurs" class="gallery-item" value="4">
                <img src="../images/bewerbung/blockchain2018_small.png" alt="4" />
            </a>
        </div>
        <div class="three columns">
            <a href="../images/bewerbung/ipv6-2018_small.png" alt="IPv6 Kurs" class="gallery-item" value ="5">
                <img src="../images/bewerbung/ipv6-2018_small.png" alt="5"/>
            </a>
        </div>
        <div class="three columns">
            <a href="../images/bewerbung/MS_Word_small.png" alt="MS Office Specialist" class="gallery-item" value="6">
                <img src="../images/bewerbung/MS_Word_small_90.png" alt="6" />
            </a>
        </div>
        <div class="three columns">
            <a href="../images/bewerbung/CCNA_NF_small.png" alt="Cisco NF Kurs" class="gallery-item" value="7">
                <img src="../images/bewerbung/CCNA_NF_small.png" alt="7" />
            </a>
        </div>
    </div>
    <div class="row">       
        <div class="three columns">
            <a href="../images/bewerbung/CCNA_WAN_small.png" alt="Cisco WAN Kurs" class="gallery-item" value="8">
                <img src="../images/bewerbung/CCNA_WAN_small.png" alt="8"/>
            </a>
        </div>
            <div class="nine columns">
                <p><br/><a href="../index.php" >Home</a></p>
            </div>
        <!--
        <div class="nine columns">
			<p><br/><a href="https://gitlab.com/jobarg/sammelkatalog-app.git" target="_blank">Studienarbeit: App-Entwicklung</a></p>
        </div>
        -->
    </div>
</div>