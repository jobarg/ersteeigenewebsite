       
    <div class="container" id="deckblatt">
        <div class="cover">            
            <img src="../images/bewerbung/image001.png" class="photo" alt="Foto Bewerber"/>  
            <img src="../images/bewerbung/placeholder-hi.png" class="photo-image" alt="Foto Bewerber"/>                  
        </div>     
               
        <div class="bewerber">            
            <strong class="public">public class</strong> Bewerber
            <br>{<br>

            <div class="content">
                Bewerber bewerber = 
                <strong class="public">new</strong> Bewerber();
                <br>
                String 
                <strong class="variable">Name</strong> = 
                <strong class="parameter">"Johann Bargen";</strong><br>
                Date <strong class="variable">Geburtstag</strong> = <strong class="parameter">"28-09-1988";</strong><br>
                String <strong class="variable">Adresse</strong> = <strong class="parameter">"Lipper Hellweg 235";</strong><br>
                String <strong class="variable">PLZ</strong> = <strong class="parameter">"33605 Bielefeld";</strong><br>
                Int <strong class="variable">Telefon</strong> = <strong class="parameter">"0176 63424009";</strong>
            </div>
            }                              
        </div>        
    </div>    


