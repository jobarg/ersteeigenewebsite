<section class="contact" id="contact">
  <div class="container text-center">
    <h3>Kontaktformular</h3>
    <p class="text-muted font-italic">Hinterlasse mir doch eine Nachricht!</p>
    <?php if(isset($_GET['contact']) && $_GET['contact'] == 'success'): ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Erfolgreich verschickt!</strong> Ihre Anfrage wird zeitnah bearbeitet.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <?php endif; ?>
    <form method="POST" action="./inc/sendContact.php">
      <div class="row">
        <div class="col-12 col-sm-6">
          <input class="form-control mb-3" type="text" name="name" placeholder="Dein Name *" required />
          <input class="form-control mb-3" type="email" name="email" placeholder="Deine E-Mail *" required />
          <input class="form-control mb-3 mb-sm-0" type="text" name="subject" placeholder="Betreff *" required />
        </div>
        <div class="col-12 col-sm-6">
          <textarea class="form-control contact-message" name="message" placeholder="Deine Nachricht *" required></textarea>
        </div>
      </div>
      <div class="text-center">
        <button type="submit" class="mt-3 btn btn-primary">Nachricht abschicken!</button>
      </div>
    </form>
  </div>
</section>
