"use strict";

let value;
let image;

function removeLB(){
    $("#lightbox").remove();
    $("#lightbox__overlay-container").remove();
}

function lb__loadImage(value, image){    
    console.log(value);
    let newSrc = $('.gallery-item[value='+value+']');
    $('.caption').text((newSrc).attr("alt"));
    $(image).attr("src", (newSrc).attr("href"));    
}

jQuery.fn.lightbox = function() {

    $(this).click(function(event) {
        event.preventDefault();
        value = $(this).attr("value");   
        
        let overlay = $.parseHTML("<div id='lightbox'></div>");
        $("body").append(overlay);       

        image = $.parseHTML("<img id='lightbox__overlay-image'>");
        let src = $(this).attr("href");
        $(image).attr("src", src);
        
        let container = $.parseHTML("<div id='lightbox__overlay-container'></div>");
        $(overlay).prepend(container);

        $(container).click(function() {
            removeLB();
        });

        let text = $(this).attr("alt");
        let caption = $.parseHTML("<div class='caption'>"+text+"</div><span class='close'>&times;</span>");
        let buttons = $.parseHTML("<a class='lb__prev lb__button'>&#10094;</a><a class='lb__next lb__button'>&#10095;</a>");   
        
        $(container).append(image)
        $(container).prepend(caption);
        $(overlay).append(buttons);        
        
        $(".close").click(function(){
            removeLB();
        });  

        $(".lb__button").click(function(){
            if($(this).attr('class')== "lb__next lb__button"){
                value++;
                //.gallery-item lÃ¤nge auslesen und benutzen
                if(value>8){
                    value=0;
                }
            }else{
                value--;
                if(value<0){
                    value=8;
                }
            }        
            lb__loadImage(value, image);        
        });        
    });   

    $(document).keydown(function(event) {        
        if (event.key == "Escape" || event.key == "Esc") {
           removeLB();
        }
        if(event.key == "ArrowRight" || event.key =="Right"){
            value++;
            if(value>8){
                value=0;
            } 
            lb__loadImage(value, image);
        }
        if(event.key == "ArrowLeft" || event.key == "Left"){
            value--;
            if(value<0){
                value=8;
            }
            lb__loadImage(value, image);
        }
    });
    return this;    
}