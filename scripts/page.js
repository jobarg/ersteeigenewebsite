"use strict";

let i=0;
let title = document.title;

function divDisplay(show){  
    $('#deckblatt, #anschreiben, #lebenslauf, #dritte').css('display', show);               
}


function display(help){

    if(help == 4 || help == "Anhang"){
        $('#anhang').show();
        document.title="Anhang"; 
    }else{
        $('#anhang').hide();
        switch(help){
            case 0:
            case "Deckblatt":
            case title:    
            $('#deckblatt').show();
            document.title="Deckblatt";
            break;
            case 1:
            case "Anschreiben":
            $('#anschreiben').show();
            document.title="Anschreiben";
            break;
            case 2:
            case "Lebenslauf":
            $('#lebenslauf').show();
            document.title="Lebenslauf";
            break;
            case 3:
            case "Dritte Seite":
            $('#dritte').show();
            document.title="Dritte Seite";
            break;  
        }
      
    }        
} 

jQuery.fn.page = function(intValue) {
    $(this).click(function(){

        i=(i + intValue);
        if(i<=0){
            $('#prev').css('pointerEvents', 'none');
            $('#prevarrow').css('border-bottom','22px solid grey');
            $('#asd').css('background-color', 'grey');    
            i=0;
         }
         else{
             $('#prev').css('pointerEvents', 'auto');
             $('#prevarrow').css('border-bottom','22px solid green');
             $('#asd').css('background-color', 'green'); 
         }
         if(i>=4){
             i=4;
             $('#next').css('pointerEvents', 'none');    
             $('#nextarrow').css('border-top','22px solid grey');    
             $('#asdn').css('background-color', 'grey');  
         }else{
            $('#next').css('pointerEvents', 'auto');        
            $('#nextarrow').css('border-top','22px solid green'); 
            $('#asdn').css('background-color', 'green'); 
         }    
     
         divDisplay("none"); 
         display(i);   
    });
    
   
}