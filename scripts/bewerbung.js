"use strict";

jQuery(document).ready(function($){
    $(".gallery-item").lightbox();
    $("#next").page(1);
    $("#prev").page(-1);

    $(window).resize(function(){
        let win = $(this);
        if(win.width() < 733){    
      
            divDisplay("block");
			$('#anhang').hide();
            
    
        }else{
        
            divDisplay("none");
            display(document.title);
            
        }
    })
});